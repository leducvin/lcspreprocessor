.. image:: http://gitlab.radonc.hmr/srp/lcspreprocessor/badges/master/pipeline.svg
   :target: http://gitlab.radonc.hmr/srp/lcspreprocessor/commits/master

.. image:: http://gitlab.radonc.hmr/srp/lcspreprocessor/badges/master/coverage.svg
   :target: http://gitlab.radonc.hmr/srp/lcspreprocessor/commits/master

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
    :target: https://github.com/psf/black

General information
===================

Preprocess a DICOM RTPlan for export to Elekta LCS.

Authors
-------

Vincent Leduc

Installation (linux)
--------------------
Should work on any python >= 3.6 (instructions tested on Ubuntu linux 20.04)

Make sure you have the venv package installed
  ``sudo apt install python3-venv``

Create a python virtual environment, for example
  ``python -m venv lcsenv``

Activate the virtual environment
  ``source lcsenv/bin/activate``

Install dependencies
  1. ``pip install wheel``
  2. ``pip install git+https://gitlab.com/leducvin/vtlyamlcfg.git``

Clone the repo
  ``git clone https://gitlab.com/leducvin/lcspreprocessor.git``

  This will clone the repository in a local ``lcspreprocessor`` directory.

Install lcspreprocessor in the virtual environment
  ``pip install -e ./lcspreprocessor``

You can now use the ``lcspreprocessor`` command.
  ``lcspreprocessor --help``

Installation (Windows)
----------------------

Install git for Windows
  https://git-scm.com/download/win

  Just use all default values in the installer.

Install python for Windows (python >= 3.6)
  https://www.python.org/downloads/

  Click ``Download Python 3.9.0`` button.

  During installation:
    - Check ``Add Python 3.9 to PATH``
    - Click ``Customize installation``, click ``Next``
    - Check ``Install for all users``
    - Click ``Install``
    - Enter administrator user and password if prompted.

In a command prompt (``cmd``), create a virtual environment:
  ``python -m venv lcsenv``

Activate the virtual environment
  ``.\lcsenv\Scripts\activate.bat``

Install dependencies
  1. ``pip install wheel``
  2. ``pip install git+https://gitlab.com/leducvin/vtlyamlcfg.git``

Clone the repo
  ``git clone https://gitlab.com/leducvin/lcspreprocessor.git``

  This will clone the repository in a local ``lcspreprocessor`` directory.

Install lcspreprocessor in the virtual environment
  ``pip install -e .\lcspreprocessor``

You can now use the ``lcspreprocessor`` command.
  ``lcspreprocessor --help``

Configuration
-------------

Edit the ``lcspreprocessor/src/lcspreprocessor/config.yaml`` file. Change the ``machine_models`` mapping to suit your needs.

Usage
-----

See help text by calling ``lcspreprocessor --help``.

Features
--------

- Supports Agility, Beam Modulator and MLCi beam limiting devices.
- Command-line interface, writes processed files under an ``LCS`` subdirectory.
