#!/usr/bin/env python

import os

from click.testing import CliRunner
import pydicom

import lcspreprocessor


try:
    from vtlutils.log import get_logger

    lcspreprocessor.main.logger = get_logger("lcspreprocessor")
except ImportError:
    pass


"""Tests for `lcspreprocessor` package."""

_thisdir = os.path.dirname(os.path.realpath(__file__))


def test_preprocess():
    ds = pydicom.read_file("{}/resources/agility.dcm".format(_thisdir))
    lcspreprocessor.preprocess(ds, machine_name="Salle 1")

    ds = pydicom.read_file("{}/resources/mlci.dcm".format(_thisdir))
    lcspreprocessor.preprocess(ds, machine_name="Salle 9")

    ds = pydicom.read_file("{}/resources/bm.dcm".format(_thisdir))
    lcspreprocessor.preprocess(ds, machine_name="Salle 11")


def test_cli():
    runner = CliRunner()
    result = runner.invoke(
        lcspreprocessor.cli.main,
        [
            "--verbose",
            "{}/resources/agility.dcm".format(_thisdir),
        ],
    )
    try:
        assert result.exit_code == 0
    except AssertionError:
        print(result.output)
        print(result.exception)
        print(result.exc_info)
        raise

    runner = CliRunner()
    result = runner.invoke(
        lcspreprocessor.cli.main,
        [
            "--verbose",
            "--machine-model",
            "Beam Modulator",
            "{}/resources/bm.dcm".format(_thisdir),
        ],
    )
    try:
        assert result.exit_code == 0
    except AssertionError:
        print(result.output)
        print(result.exception)
        print(result.exc_info)
        raise

    runner = CliRunner()
    result = runner.invoke(
        lcspreprocessor.cli.main,
        [
            "--verbose",
            "--machine-name",
            "Salle 11",
            "{}/resources/bm.dcm".format(_thisdir),
        ],
    )
    try:
        assert result.exit_code == 0
    except AssertionError:
        print(result.output)
        print(result.exception)
        print(result.exc_info)
        raise

    runner = CliRunner()
    result = runner.invoke(
        lcspreprocessor.cli.main,
        [
            "--verbose",
            "--enforce-minimum-gap",
            "{}/resources/mlci.dcm".format(_thisdir),
        ],
    )
    try:
        assert result.exit_code == 0
    except AssertionError:
        print(result.output)
        print(result.exception)
        print(result.exc_info)
        raise


def test_version():
    assert lcspreprocessor.__version__ is not None
