.. highlight:: shell

============
Installation
============

It is recommended to install ``lcspreprocessor`` in a virtual
environment.

User mode
---------

If you only want to use ``lcspreprocessor`` and don't want or need
to modify it, it can be installed with:

.. code-block:: console

    $ pip install --extra-index-url file:///radonc/dev/python/packages/wheels/simple lcspreprocessor

This is the preferred method to install ``lcspreprocessor``, as it
will always install the most recent stable release. Furthermore all dependencies
of ``lcspreprocessor`` will be installed as well.

The ``--extra-index-url`` can be omitted if written in the pip config file
 ``~/.config/pip/pip/conf`` as:

.. code-block:: console

    [global]
    extra-index-url = file:///radonc/dev/packages/wheels/simple


Development mode
----------------

If you think you may need to make modifications to ``lcspreprocessor``,
you have to install it in development mode.

First, clone the repository:

.. code-block:: console

    $ git clone /radonc/dev/repos/lcspreprocessor

Packages needed for developping ``lcspreprocessor`` can be installed
by:

.. code-block:: console

    $ make install-dev


Then, from the cloned repo run:

.. code-block:: console

    $ make devel

From this point any modification made to the cloned repo
``lcspreprocessor`` will take effect in your virtual environment.

In the case you do make significant modifications, you may consider contributing
your changes to the main codebase.
