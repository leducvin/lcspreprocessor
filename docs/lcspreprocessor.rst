Package documentation
=====================

.. automodule:: lcspreprocessor
    :members:
    :special-members: __init__, __call__
    :imported-members:
    :undoc-members:
    :show-inheritance:
