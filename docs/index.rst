=========================================
Welcome to lcspreprocessor documentation!
=========================================

Contents:

.. toctree::
   :maxdepth: 1

   readme
   installation
   lcspreprocessor
   history


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
