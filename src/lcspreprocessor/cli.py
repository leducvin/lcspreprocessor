"""Console script for lcspreprocessor."""

import glob
import logging
import os
import pathlib
from pprint import pformat

import click
import pydicom


def _set_up_logging(log_dir: str, verbose: bool) -> logging.Logger:
    """
    Set up logging.

    Args:
        log_dir: the directory where the log file will be written, can
            be ``None``.
        verbose: whether to print more logs

    """
    global logger
    logname = __name__.split(".")[0]

    try:  # pragma: no cover
        import vtlutils.log

        logger = vtlutils.log.get_logger(
            logname,
            level=logging.DEBUG if verbose else logging.WARNING,
            logfile=f"{log_dir}/{logname}.log" if log_dir else None,
        )

        logger = logging.getLogger(__name__)
        logger.propagate = True
    except ImportError:
        logger = logging.getLogger(logname)
        logger.setLevel(logging.DEBUG if verbose else logging.WARNING)

        formatter = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        )

        if log_dir:
            # create file handler
            fh = logging.FileHandler(f"{log_dir}/{logname.log}")
            fh.setFormatter(formatter)
            logger.addHandler(fh)

        # create console handler
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        logger.addHandler(ch)

    logger.info("Logging ready.")

    return logger


@click.command()
@click.option(
    "-n",
    "--machine-name",
    type=str,
    help="The Treatment Machine Name, as in the DICOM RTPlan, TPS and R&V system. If supplied, will overwrite machine name already present in the RTPlan.",
)
@click.option(
    "-m",
    "--machine-model",
    type=click.Choice(["Agility", "Beam Modulator", "MLCi"]),
    help="The machine's BLD model.",
)
@click.option(
    "-g",
    "--enforce-minimum-gap",
    is_flag=True,
    help="Enforce minimum gap between opposed leaves. If used, leaf positions will be changed to enforce a minimum gap. By default only affects MLCi.",
)
@click.option("--verbose", is_flag=True, help="Print more logs.")
@click.argument(
    "rtplans",
    type=click.Path(exists=True, file_okay=True, dir_okay=True, resolve_path=True),
    required=True,
    nargs=-1,
)
def main(rtplans, **options):
    """
    lcspreprocessor: preprocess DICOM RTPlans for exportation to Elekta
    LCS.

    RTPLANS argument: the path to a directory where RTPlans are located
    or to one or more RTPlan files. The RTPlan files must have the .dcm
    file extension.

    NOTE:

    This script only supports plans that use a single type of BLD
    model. Multiple treatment machine names can be used as along as they
    all use the same BLD model.

    \b
    - The preprocessor fills in necessary tags with default values if
      missing.
    - New UIDs are generated.
    - Referenced Structure Set is deleted.
    - Referenced Dose Sequence is deleted.
    - If provided, treatment machine name is set to the specified value.
    - BeamLimitingDeviceTypes are set to correct values for the BLD
      model.
    - If MLCs are missing (beam defined by jaws only), they are added.
    - Leaf positions limits (corner leaves) are enforced (Agility and
      MLCi).
    - For Agility, IEC61217 X jaws are set to (-200, 200).
    - If the --enforce-minimum-gap option is used, leaves are moved
      slightly in order to respect minimum gap. If this option is not
      used, leaf positions are not modified, except for corner leaves on
      MLCi and Agility,if they exceed limits.

    The processed files are saved under a subdirectory LCS.

    NOTE:

    If you are not using the --machine-model option, you should
    customize the included src/lcsproprocessor/config.yaml file. You
    must set the mapping between machine names and BLD model correctly.
    The script will then be able to find the correct BLD model from the
    treatment machine name in the DICOM RTPlan.

    EXAMPLES:

    \b
    lcspreprocessor --verbose /a/rtplan/file.dcm
      (doesn't change TreatmentMachineName, uses exisiting
      TreatmentMachineName value in the RTPlan file and the machine name
      to BLD model mapping found in config.yaml)

    \b
    lcspreprocessor --verbose --machine-name "Room 1" ./a/rtplan/file1.dcm ./a/rtplan/file2.dcm
      (sets TreatmentMachineName to `Room 1` in the RTPlan, uses the
      mapping found in config.yaml to get BLD model)

    \b
    lcspreprocessor --verbose --machine-model MLCi ./some/dir/with/rtplans
      (doesn't change TreatmentMachineName, doesn't use the mapping in
      the config.yaml file, directly uses the BLD model name as
      specified)

    EXPORTING TO LCS:

    The processed files can then be exported to the using DICOM Toolkit's
    (dcmtk) `storescu` command, e.g.:

    \b
    storescu -v -aet <calling AE Title> -aec EOS_RTD <linac NSS IP address> 104 rtplanfile1.dcm rtplanfile2.dcm
    """
    _set_up_logging(log_dir=None, verbose=options["verbose"])

    from .main import preprocess

    logger.debug(f"Called with {pformat(options)}")

    temp = []
    for r in rtplans:
        if os.path.isdir(r):
            temp.extend(glob.glob("{}/*.dcm".format(r)))
        elif os.path.isfile(r):
            temp.append(r)
        else:
            logger.warning("Ignoring {}: neither a file nor a directory".format(r))

    logger.debug(f"List of files to process: {pformat(temp)}")

    status = 0
    for r in temp:
        try:
            logger.debug(f"Processing {r} ...")
            ds = pydicom.read_file(r)
            preprocess(
                ds,
                machine_name=options["machine_name"],
                machine_model=options["machine_model"],
                enforce_minimum_leaf_gap=options["enforce_minimum_gap"],
            )

            fdir = f"{os.path.dirname(r)}/LCS"
            temppath = pathlib.Path(fdir)
            temppath.mkdir(parents=True, exist_ok=True)
            ds.save_as(f"{fdir}/{os.path.basename(r)}", write_like_original=False)
            logger.info(f"Wrote {fdir}/{os.path.basename(r)}")
        except Exception:  # pragma: no cover
            status = 1
            logger.exception(f"Error processing {r}")
    exit(status)
