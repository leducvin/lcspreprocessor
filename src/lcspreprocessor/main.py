"""Main module."""

import logging
from pprint import pformat

import pydicom

from lcspreprocessor.config import cfg


logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def _check_equal(iterator):
    """
    Check that items in the iterator are all equal.

    Returns a tuple of (True/False, value of the first item).

    Works with sequences or iterators.
    """
    iterator = iter(iterator)
    try:
        first = next(iterator)
    except StopIteration:
        return True, first
    return all(first == rest for rest in iterator), first


def unique(items):
    """Returns an item if all items equal, else raise exception"""
    u, v = _check_equal(items)
    if u:
        return v
    else:
        raise ValueError(f"items are not the same: {pformat(items)}")


def _preprocess(
    rtplan,
    machine_name=None,
    machine_model=None,
    enforce_minimum_leaf_gap=False,
    minimum_leaf_gap=None,
):
    """preprocess rtplan dataset."""

    if machine_name is not None and machine_model is None:
        try:
            cfg["machine_models"][machine_name]
        except KeyError:
            raise ValueError(
                "machine_name not recognized and machine_model was not specified."
            )

    temp_machine_models = []
    for m in [_.TreatmentMachineName for _ in rtplan.BeamSequence]:
        try:
            temp_machine_models.append(cfg["machine_models"][m])
        except KeyError:
            if machine_name is None or machine_model is None:
                logger.error(
                    f"Plan uses an unknown machine '{m}'' and neither machine name nor machine_model was specified."
                )
                raise
            else:
                logger.warning(
                    f"Plan uses machine '{m}' but machine_name='{machine_name}' and machine_model='{machine_model}' were specified."
                )
                temp_machine_models.append(cfg["machine_models"][machine_name])

    if machine_model:
        if machine_model not in cfg["models"]:
            raise ValueError(
                "{} BLD model is not known. Check config.".format(machine_model)
            )
    else:
        try:
            machine_model = unique(temp_machine_models)
        except ValueError:
            if machine_name:
                machine_model = cfg["machine_models"][machine_name]
            else:
                logger.error(
                    "Plan uses machines that use multiple BLD models. This is not supported."
                )
                raise

    # PatientBirthDate
    try:
        if len(str(rtplan[0x0010, 0x0030].value)) == 0:
            rtplan[0x0010, 0x0030].value = cfg["patientbirthdate"]
    except Exception:
        rtplan.PatientBirthDate = cfg["patientbirthdate"]

    # OperatorsName
    try:
        if len(str(rtplan[0x0008, 0x1070].value)) == 0:
            rtplan[0x0008, 0x1070].value = cfg["operatorsname"]
    except Exception:
        rtplan.OperatorsName = cfg["operatorsname"]

    # ReferringPhysicianName
    try:
        if len(str(rtplan[0x0008, 0x0090].value)) == 0:
            rtplan[0x0008, 0x0090].value = cfg["referringphysicianname"]
    except Exception:
        rtplan.ReferringPhysicianName = cfg["referringphysicianname"]

    # AccessionNumber
    try:
        if len(str(rtplan[0x0008, 0x0050].value)) == 0:
            rtplan[0x0008, 0x0050].value = cfg["accessionnumber"]
    except Exception:
        rtplan.AccessionNumber = cfg["accessionnumber"]

    # StudyTime
    try:
        if len(str(rtplan[0x0008, 0x0030].value)) == 0:
            rtplan[0x0008, 0x0030].value = cfg["studytime"]
    except Exception:
        rtplan.StudyTime = cfg["studytime"]

    # StudyID
    try:
        if len(str(rtplan[0x0020, 0x0010].value)) == 0:
            rtplan[0x0020, 0x0010].value = cfg["studyid"]
    except Exception:
        rtplan.StudyID = cfg["studyid"]

    try:
        rtplan[0x300A, 0x000C].value = "TREATMENT_DEVICE"
    except Exception:
        logger.exception("Error")

    # Delete Referenced Structure Set Sequence
    try:
        del rtplan[0x300C, 0x0060]
    except Exception:
        pass

    # Delete Referenced Dose Sequence
    try:
        del rtplan[0x300C, 0x0080]
    except Exception:
        pass

    if "RTPlanLabel" not in rtplan or not rtplan.RTPlanLabel:
        rtplan.RTPlanLabel = "NOLABEL"

    try:
        # Generate new UIDs before sending
        rtplan[0x0008, 0x0018].value = pydicom.uid.generate_uid()
        rtplan[0x0020, 0x000D].value = pydicom.uid.generate_uid()
        rtplan[0x0020, 0x000E].value = pydicom.uid.generate_uid()
    except Exception:
        logger.exception("Error")

    for bs in rtplan.BeamSequence:
        if machine_name:
            bs.TreatmentMachineName = machine_name

        for blds in bs.BeamLimitingDeviceSequence:
            if not blds.RTBeamLimitingDeviceType.startswith("MLC"):
                if blds.RTBeamLimitingDeviceType.endswith("X"):
                    blds.RTBeamLimitingDeviceType = cfg["Xjawtype"][machine_model]
                if blds.RTBeamLimitingDeviceType.endswith("Y"):
                    blds.RTBeamLimitingDeviceType = cfg["Yjawtype"][machine_model]

        # If sending to agility and MLC is not used in Pinnacle, LCS
        # will complain of missing MLCX BLD sequence
        if machine_model == "Agility":
            if "MLCX" not in [
                x.RTBeamLimitingDeviceType for x in bs.BeamLimitingDeviceSequence
            ]:
                # TODO create sequence for MLCX with proper leaf
                # positions add it to CP BLD Sequence as well.

                ds = pydicom.dataset.Dataset()
                ds.RTBeamLimitingDeviceType = "MLCX"
                ds.SourceToBeamLimitingDeviceDistance = "373"
                ds.NumberOfLeafJawPairs = cfg["nleaves"][machine_model]
                ds.LeafPositionBoundaries = cfg["boundaries"][machine_model]

                bs.BeamLimitingDeviceSequence.append(ds)

                for k, _cp in enumerate(bs.ControlPointSequence):
                    if hasattr(
                        bs.ControlPointSequence[k], "BeamLimitingDevicePositionSequence"
                    ):
                        for _j, bld in enumerate(
                            # fmt: off
                            bs.ControlPointSequence[k].BeamLimitingDevicePositionSequence
                            # fmt: on
                        ):
                            if bld.RTBeamLimitingDeviceType == "ASYMX":
                                leaf_pos = bld.LeafJawPositions

                        ds = pydicom.dataset.Dataset()
                        ds.RTBeamLimitingDeviceType = "MLCX"
                        ds.LeafJawPositions = []

                        for _l in range(0, 80):
                            ds.LeafJawPositions.append(leaf_pos[0])
                        for _l in range(80, 160):
                            ds.LeafJawPositions.append(leaf_pos[1])

                        # fmt: off
                        bs.ControlPointSequence[k].BeamLimitingDevicePositionSequence.append(ds)
                        # fmt: on

        for k, _cp in enumerate(bs.ControlPointSequence):
            if hasattr(
                bs.ControlPointSequence[k], "BeamLimitingDevicePositionSequence"
            ):
                for h, bld in enumerate(
                    bs.ControlPointSequence[k].BeamLimitingDevicePositionSequence
                ):

                    if not bld.RTBeamLimitingDeviceType.startswith("MLC"):
                        # fmt: off
                        if bld.RTBeamLimitingDeviceType.endswith("X"):
                            bld.RTBeamLimitingDeviceType = cfg["Xjawtype"][machine_model]
                        if bld.RTBeamLimitingDeviceType.endswith("Y"):
                            bld.RTBeamLimitingDeviceType = cfg["Yjawtype"][machine_model]
                        # fmt: on

                    if bld.RTBeamLimitingDeviceType.startswith("MLC"):
                        for j, lp in enumerate(
                            bs.ControlPointSequence[k]
                            .BeamLimitingDevicePositionSequence[h]
                            .LeafJawPositions
                        ):
                            # Correct for leaves outside of possible range
                            orig = (
                                bs.ControlPointSequence[k]
                                .BeamLimitingDevicePositionSequence[h]
                                .LeafJawPositions[j]
                            )
                            if machine_model in cfg["indiv_leaf_limits"]:
                                try:
                                    lim = min(
                                        cfg["indiv_leaf_limits"][machine_model][j],
                                        cfg["X_limit"][machine_model],
                                    )
                                except KeyError:
                                    lim = cfg["X_limit"][machine_model]
                            else:
                                lim = cfg["X_limit"][machine_model]
                            if abs(orig) > lim:
                                new = -lim if orig < 0 else lim
                                # fmt: off
                                bs.ControlPointSequence[k].BeamLimitingDevicePositionSequence[h].LeafJawPositions[j] = new
                                # fmt: on
                                logger.warning(
                                    "Leaf position for leaf index {} was changed from {} to {} in order to respect leaf position limits.".format(
                                        j, orig, new
                                    )
                                )

                    if machine_model == "Agility":
                        # Must set X diaphragms to fixed [-200,200]
                        # positions for Agility for plan coming from
                        # Pinnacle.
                        if (
                            bld.RTBeamLimitingDeviceType == "X"
                            or bld.RTBeamLimitingDeviceType == "ASYMX"
                        ):
                            logger.info(
                                "Changing X jaws positions to (-200, 200) for Agility."
                            )
                            # fmt: off
                            bs.ControlPointSequence[k].BeamLimitingDevicePositionSequence[h].LeafJawPositions[0] = "-200.0"
                            bs.ControlPointSequence[k].BeamLimitingDevicePositionSequence[h].LeafJawPositions[1] = "200.0"
                            # fmt: on

        # Loop through leafpairs to enforce minimum gap
        # Deactivate this for Beam Modulator, as it interferes with
        # closed leaf position.
        if enforce_minimum_leaf_gap and machine_model != "Beam Modulator":
            if minimum_leaf_gap is None:
                minimum_leaf_gap = cfg["minimum_leaf_gap"][machine_model]
            for k, _cp in enumerate(bs.ControlPointSequence):
                if hasattr(
                    bs.ControlPointSequence[k], "BeamLimitingDevicePositionSequence"
                ):
                    for h, bld in enumerate(
                        bs.ControlPointSequence[k].BeamLimitingDevicePositionSequence
                    ):

                        if not bld.RTBeamLimitingDeviceType.startswith("MLC"):
                            # fmt: off
                            if bld.RTBeamLimitingDeviceType.endswith("X"):
                                bld.RTBeamLimitingDeviceType = cfg["Xjawtype"][machine_model]
                            if bld.RTBeamLimitingDeviceType.endswith("Y"):
                                bld.RTBeamLimitingDeviceType = cfg["Yjawtype"][machine_model]
                            # fmt: on

                        if bld.RTBeamLimitingDeviceType.startswith("MLC"):
                            for j, lp in enumerate(
                                bs.ControlPointSequence[k]
                                .BeamLimitingDevicePositionSequence[h]
                                .LeafJawPositions
                            ):
                                if j >= cfg["nleaves"][machine_model]:
                                    continue
                                # Enforce minimum leaf gap
                                g = (
                                    bs.ControlPointSequence[k]
                                    .BeamLimitingDevicePositionSequence[h]
                                    .LeafJawPositions[j + cfg["nleaves"][machine_model]]
                                    - lp
                                )
                                assert g >= 0
                                # logger.debug(f"{j}, {j + cfg['nleaves'][machine_model]}: gap={g} mm", minimum={cfg['minimum_leaf_gap'][machine_model]})
                                if g < minimum_leaf_gap:
                                    # fmt: off
                                    d = (minimum_leaf_gap - g) * 0.5
                                    assert d > 0
                                    bs.ControlPointSequence[k].BeamLimitingDevicePositionSequence[h].LeafJawPositions[j] -= d
                                    bs.ControlPointSequence[k].BeamLimitingDevicePositionSequence[h].LeafJawPositions[j + cfg["nleaves"][machine_model]] += d
                                    # fmt: on
                                    logger.warning(
                                        "Leaf positions for leaves index {} and {} were changed by {:.2f} mm in order to respect minimum leaf gap of {} mm.".format(
                                            j,
                                            j + cfg["nleaves"][machine_model],
                                            d,
                                            minimum_leaf_gap,
                                        )
                                    )


def preprocess(*args, **kwargs):
    """preprocess rtplan dataset."""
    try:
        _preprocess(*args, **kwargs)
    except Exception:  # pragma: no cover
        logger.exception("Error.")
        raise
