"""Top-level package for lcspreprocessor."""

from lcspreprocessor._version import __version__  # noqa: F401
import lcspreprocessor.cli
from lcspreprocessor.main import *  # noqa: F401, F403


__author__ = "Vincent Leduc"
