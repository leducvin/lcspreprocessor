"""Config module."""

import os

import numpy

from vtlyamlcfg import YAMLConfig


_thisdir = os.path.dirname(os.path.realpath(__file__))


class PreprocessorConfig(YAMLConfig):

    """A helper object for reading the YAML configuration."""

    def read(self, *args, **kwargs):
        super(PreprocessorConfig, self).read(*args, **kwargs)

        self._cfg["boundaries"] = dict()
        for m in self["models"]:
            n = self["nleaves"][m]
            y = self["Y_limit"][m]
            temp = numpy.linspace(-y, y, n + 1)
            self._cfg["boundaries"][m] = [str(t) for t in temp]


cfg = PreprocessorConfig()
cfg.read("{}/config.yaml".format(_thisdir))
