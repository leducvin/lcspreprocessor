#!/usr/bin/env python

"""The setup script."""
import os
import sys

from setuptools import find_packages, setup


PACKAGE = "lcspreprocessor"

# Fetch version from git tags, and write to version.txt.
version_txt = os.path.join(os.path.dirname(__file__), "src/%s/version.txt" % PACKAGE)
if sys.argv[1] != "develop":
    exec(open("src/%s/_version.py" % PACKAGE).read())
    version_git = get_version(version_txt)  # noqa F821
else:
    version_git = "devel"

# write version to version.txt
with open(version_txt, "w") as fh:
    fh.write(version_git)

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()


# packages needed by this project
requirements = [
    "click",
    "pydicom",
    "numpy",
    "vtlyamlcfg",
]

# extra requirements, enables definition of conditional dependencies
# see:
# https://wheel.readthedocs.io/en/stable/index.html#defining-conditional-dependencies
extra_requirements = {
    # ':python_version=="2.7"': ['Polygon2', 'mock'],
    # ':python_version>"2.7"': ['Polygon3'],
}

setup(
    name=PACKAGE,
    version=version_git,
    description="Preprocess a DICOM RTPlan for export to Elekta LCS.",
    long_description=readme + "\n\n" + history,
    author="Vincent Leduc",
    url="https://gitlab.com/leducvin/lcspreprocessor",
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    include_package_data=True,
    install_requires=requirements,
    extras_require=extra_requirements,
    entry_points={"console_scripts": ["lcspreprocessor=lcspreprocessor.cli:main"]},
)

# once setup has finished, get rid of version.txt
try:
    os.remove(version_txt)
except IOError:
    pass
